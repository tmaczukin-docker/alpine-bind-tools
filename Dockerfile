ARG ALPINE_VERSION
FROM alpine:${ALPINE_VERSION}

LABEL maintainer="Tomasz Maczukin <tomasz@maczukin.pl>"

RUN apk add --no-cache bind-tools

